import { useRef } from 'react';
import { useEffect } from 'react';
import axios from 'axios';
import qs from 'qs';
import iceconfig from './iceconfig.json';

const RTSP_SERVER = `http://192.168.3.31:8083/stream`;

type Props = {
  suuid: string;
};

export default ({ suuid }: Props) => {
  const videoElemRef = useRef(null);
  useEffect(() => {
    if (!suuid) return;
    const stream = new MediaStream();
    const pc = new RTCPeerConnection(iceconfig);
    pc.onnegotiationneeded = handleNegotiationNeededEvent;
    pc.ontrack = (event) => {
      stream.addTrack(event.track);
      if (!videoElemRef.current) throw new Error('no videoElem');
      const videoElem: HTMLVideoElement = videoElemRef.current;
      videoElem.srcObject = stream;
    };
    async function handleNegotiationNeededEvent() {
      let offer = await pc.createOffer();
      await pc.setLocalDescription(offer);
      getRemoteSdp(pc, suuid);
    }
    getCodecInfo(pc, suuid);

    return () => {
      try {
        stopStreamed(stream);
        pc.close();
      } catch (error) {}
    };
  }, [suuid]);
  return (
    <video
      ref={videoElemRef}
      style={{ width: '600px' }}
      autoPlay
      muted
      controls
    ></video>
  );
};

interface I_typeData {
  Type: string;
}

function getCodecInfo(pc: RTCPeerConnection, suuid: string) {
  axios.get(`${RTSP_SERVER}/codec/${suuid}`).then((response) => {
    const data = response.data;
    data.forEach((data: I_typeData) => {
      pc.addTransceiver(data.Type, {
        direction: 'sendrecv',
      });
    });
  });
}

function getRemoteSdp(pc: RTCPeerConnection, suuid: string) {
  if (!pc.localDescription) {
    console.warn(`👹Cannot getRemoteSdp`);
    return;
  }
  axios
    .post(
      `${RTSP_SERVER}/receiver/${suuid}`,
      {
        suuid: suuid,
        data: btoa(pc.localDescription.sdp),
      },
      {
        transformRequest: [
          function (data) {
            return qs.stringify(data, {
              arrayFormat: 'brackets',
            });
          },
        ],
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      },
    )
    .then((response) => {
      const data = response.data;
      try {
        pc.setRemoteDescription(
          new RTCSessionDescription({
            type: 'answer',
            sdp: atob(data),
          }),
        );
      } catch (e) {
        console.warn(e);
      }
    });
}

function stopStreamed(stream: MediaStream) {
  const tracks = stream.getTracks();
  tracks.forEach(function (track) {
    track.stop();
  });
}
