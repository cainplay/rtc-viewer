import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import fs from 'fs';

const https = process.env.SSL_KEY_FILE
  ? {
      key: fs.readFileSync(process.env.SSL_KEY_FILE),
      cert: fs.readFileSync(process.env.SSL_CERT_FILE),
    }
  : false;

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    host: '0.0.0.0',
    https,
  },
  plugins: [react()],
});
